#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

#include "Consts.h"
#include "UtilityFunctions.h"

void shiftDFT(cv::Mat& fImage) {
	cv::Mat temp, quadrant0, quadrant1, quadrant2, quadrant3;

	// Cropping the image if it has an odd number of rows or columns
	fImage = fImage(cv::Rect(0, 0, fImage.cols & -2, fImage.rows & -2));

	int centerX = fImage.cols / 2;
	int centerY = fImage.rows / 2;

	// Shifting the fourier image so it is centered. 
	quadrant0 = fImage(cv::Rect(0, 0, centerX, centerY));
	quadrant1 = fImage(cv::Rect(centerX, 0, centerX, centerY));
	quadrant2 = fImage(cv::Rect(0, centerY, centerX, centerY));
	quadrant3 = fImage(cv::Rect(centerX, centerY, centerX, centerY));

	quadrant0.copyTo(temp);
	quadrant3.copyTo(quadrant0);
	temp.copyTo(quadrant3);

	quadrant1.copyTo(temp);
	quadrant2.copyTo(quadrant1);
	temp.copyTo(quadrant2);
}

// Based on https://docs.opencv.org/2.4/doc/tutorials/core/discrete_fourier_transform/discrete_fourier_transform.html
auto applyDFT(cv::Mat sourceImage, std::string folderPath, bool writeToFileAndDisplay = true) {
	cv::Mat paddedImage;

	// Expanding image to optimal size (As DFT's prefers images that are multiples 2, 3 and 5)
	auto optimalWidth = cv::getOptimalDFTSize(sourceImage.cols);
	auto optimalHeight = cv::getOptimalDFTSize(sourceImage.rows);
	cv::copyMakeBorder(sourceImage, paddedImage, 
					   0, optimalHeight - sourceImage.rows, 0, optimalWidth - sourceImage.cols, 
					   cv::BORDER_CONSTANT, cv::Scalar::all(0));

	// Preparing data storage for both complex and real components
	std::vector<cv::Mat> planes{cv::Mat_<float>(paddedImage), cv::Mat::zeros(paddedImage.size(), CV_32F)};
	cv::Mat fourierImage;
	cv::merge(planes.data(), 2, fourierImage);

	// Perform DFT
	cv::dft(fourierImage, fourierImage);

	// Display real/imaginary components
	// The real component is now in planes[0] and the imaginary component is in planes[1]
	cv::split(fourierImage, planes);

	if (writeToFileAndDisplay) {
		displayScaledImage(planes[0], "Fourier Real Component");
		displayScaledImage(planes[1], "Fourier Imaginary Component");
		cv::imwrite("./processedImages/" + folderPath + "realComponent.png", planes[0]);
		cv::imwrite("./processedImages/" + folderPath + "imaginaryComponent.png", planes[1]);
	}

	// Calculate magnitude of the fourier image. 
	cv::Mat fourierMagnitude;
	cv::magnitude(planes[0], planes[1], fourierMagnitude);

	// Changing to a logarithmic scale for display purposes. 
	fourierMagnitude += cv::Scalar::all(1);
	cv::log(fourierMagnitude, fourierMagnitude);

	// Cropping away the border padding as well as reordering the quadrants so that the origin is in the center. 
	// The spectrum is cropped if it has an odd number of rows or columns. 
	fourierMagnitude = fourierMagnitude(cv::Rect(0, 0, fourierMagnitude.cols & -2, fourierMagnitude.rows & -2));
	auto centerX = fourierMagnitude.cols / 2;
	auto centerY = fourierMagnitude.rows / 2;

	shiftDFT(fourierMagnitude);

	// Normalization for display purposes.
	cv::normalize(fourierMagnitude, fourierMagnitude, 0, 255, CV_MINMAX, CV_8UC1);

	// Display Magnitude
	if (writeToFileAndDisplay) {
		displayScaledImage(fourierMagnitude, "Fourier Magnitude");
		cv::imwrite("./processedImages/" + folderPath + "magnitude.png", fourierMagnitude);
	}

	return std::pair<std::vector<cv::Mat>, cv::Mat>(planes, fourierMagnitude);
}

// Based on https://stackoverflow.com/questions/19761526/how-to-do-inverse-dft-in-opencv
auto applyInverseDFT(std::pair<std::vector<cv::Mat>, cv::Mat> transformedImage, int originalWidth, int originalHeight, std::string folderPath) {
	cv::Mat reconstructedImage;

	cv::Mat fourierImage;
	cv::merge(transformedImage.first.data(), 2, fourierImage);

	cv::idft(fourierImage, reconstructedImage, cv::DFT_REAL_OUTPUT);
	cv::normalize(reconstructedImage, reconstructedImage, 0, 255, CV_MINMAX, CV_8UC1);

	// The reconstructed image does not necessarily have the same resolution due to padding so we need to crop any eventual padding borders. 
	reconstructedImage = reconstructedImage(cv::Rect(0, 0, originalWidth, originalHeight));

	cv::imwrite("./processedImages/" + folderPath + "reconstruction.png", reconstructedImage);

	return reconstructedImage;
}

void compareImages(cv::Mat source, cv::Mat reconstructed, std::string folderPath) {
	try {
		cv::Mat imageDifference;

		// A 100% similar image should be all black. 
		cv::absdiff(source, reconstructed, imageDifference);
		displayScaledImage(imageDifference, "Image difference between source and reconstruction");
		cv::imwrite("./processedImages/" + folderPath + "imageDifference.png", imageDifference);
	}
	catch (cv::Exception& e) {
		std::cerr << e.what() << std::endl;
	}
}

// Based on http://answers.opencv.org/question/36309/opencv-gaussian-noise/
auto applyGaussianNoise(cv::Mat sourceImage, int mean, int stdDeviation, float noiseOpacity = 1) {
	auto processedImage = sourceImage.clone();

	cv::Mat noiseMatrix = cv::Mat(processedImage.size(), sourceImage.flags);
	cv::normalize(processedImage, processedImage, 0, 255, CV_MINMAX, sourceImage.flags);
	cv::randn(noiseMatrix, mean, stdDeviation);
	cv::normalize(noiseMatrix, noiseMatrix, 0, 255, CV_MINMAX, sourceImage.flags);
	noiseMatrix *= noiseOpacity;
	
	processedImage += noiseMatrix;
	cv::normalize(processedImage, processedImage, 0, 255, CV_MINMAX, sourceImage.flags);

	return processedImage;
}

void createGaussianDFTFilter(cv::Mat& outputMat, int kernelSize, double sigma) {
	auto gaussianFilterX = cv::getGaussianKernel(kernelSize, sigma, CV_32F);
	auto gaussianFilterY = cv::getGaussianKernel(kernelSize, sigma, CV_32F);
	cv::Mat gaussianFilter = gaussianFilterX * gaussianFilterY.t();

	cv::copyMakeBorder(gaussianFilter, gaussianFilter,
		(outputMat.rows - gaussianFilter.rows) * 0.5,
		(outputMat.rows - gaussianFilter.rows) * 0.5 + 1,
		(outputMat.cols - gaussianFilter.cols) * 0.5,
		(outputMat.cols - gaussianFilter.cols) * 0.5 + 1,
		cv::BORDER_CONSTANT, cv::Scalar::all(0));

	cv::Mat mergeComponents[] = { gaussianFilter, gaussianFilter };
	cv::merge(mergeComponents, 2, outputMat);
}