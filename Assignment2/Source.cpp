#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

#include "ProcessingFunctions.h"
#include "UtilityFunctions.h"
#include "Consts.h"

void taskAtoD(cv::Mat sourceImage) {
	// Task a) and b)
	auto transformedImage = applyDFT(sourceImage, "tasksAD/");

	// Task c)
	auto reconstructedImage = applyInverseDFT(transformedImage, sourceImage.cols, sourceImage.rows, "tasksAD/");
	displayScaledImage(reconstructedImage, "Reconstructed Image");

	// Task d) 
	compareImages(sourceImage, reconstructedImage, "tasksAD/");
}

void taskEtoF(cv::Mat sourceImage) {
	// Task e) Comments: after adding noise, its harder to see the perpendicular lines in the magnitude. The spectrum itself seems lighter on average. 
	// Its also harder to see the various shapes in the real/imaginary components. 
	auto noisyImage = applyGaussianNoise(sourceImage, 50, 20, 0.5f);
	displayScaledImage(noisyImage, "Noisy Image");
	cv::imwrite("./processedImages/tasksEF/noisySource.png", noisyImage);
	auto transformedImage = applyDFT(noisyImage, "tasksEF/");
	
	// Task f)	
	cv::Mat fourierImage;
	cv::merge(transformedImage.first, fourierImage);

	// Acquire Gaussian filter. We clone the fourier image to keep the metadata consistent.   
	// This means that the filter will have 2 channels, but both of these will be the same. 
	auto gaussianFilter = fourierImage.clone();
	int sigma = 127;
	createGaussianDFTFilter(gaussianFilter, sigma*3, sigma);

	// Perform element by element multiplication between the spectrums of the image and the filter.
	// We shift the image to the center as the filter itself is centered as well. 
	shiftDFT(fourierImage);
	cv::mulSpectrums(fourierImage, gaussianFilter, fourierImage, 0);
	shiftDFT(fourierImage);

	cv::split(fourierImage, transformedImage.first);

	// Reconstruct filtered image
	auto reconstructedImage = applyInverseDFT(transformedImage, sourceImage.cols, sourceImage.rows, "tasksEF/");
	displayScaledImage(reconstructedImage, "Reconstructed Image");

	// Compare with source. 
	// They are of course still very different, but the filtering smoothed it out a little bit. 
	// Due to the noise, gaussian blur made the image lighter in general. 
	compareImages(sourceImage, reconstructedImage, "tasksEF/");
}

int main(int argc, char** argv) {
	auto sourceImage = cv::imread("./sourceImages/kitty.png", cv::IMREAD_GRAYSCALE);

	if (sourceImage.empty())  {
		std::cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	displayScaledImage(sourceImage, "Source Image");

	// I divided these into two functions to not entirely fill the screen with a million windows so its possible to comment out one or the other. 
	// Running both will force the second function to override any windows created by the first. 
	// taskAtoD(sourceImage);
	taskEtoF(sourceImage);

	cv::waitKey(0);

	return 0;
}