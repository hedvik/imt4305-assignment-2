#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

#include "Consts.h"

void displayScaledImage(cv::Mat image, std::string windowName) {
	cv::namedWindow(windowName, cv::WINDOW_KEEPRATIO);
	cv::resizeWindow(windowName, image.cols * IMAGE_DISPLAY_SCALE, image.rows * IMAGE_DISPLAY_SCALE);
	cv::imshow(windowName, image);
}